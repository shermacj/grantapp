﻿namespace GrantApp
{
    partial class AddProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.submitButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.nameText = new System.Windows.Forms.TextBox();
            this.overviewText = new System.Windows.Forms.TextBox();
            this.needStatementText = new System.Windows.Forms.TextBox();
            this.objectivesText = new System.Windows.Forms.TextBox();
            this.evalPlanText = new System.Windows.Forms.TextBox();
            this.budgetText = new System.Windows.Forms.TextBox();
            this.timelineText = new System.Windows.Forms.TextBox();
            this.notesText = new System.Windows.Forms.TextBox();
            this.lettersText = new System.Windows.Forms.TextBox();
            this.projectLabel = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Overview:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Need Statement:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 228);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Objectives:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 309);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Evaluation Plan:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(327, 114);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Timeline:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(327, 201);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Letters of Support:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(327, 282);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Notes:";
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(408, 351);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(75, 23);
            this.submitButton.TabIndex = 20;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(514, 351);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 21;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // nameText
            // 
            this.nameText.Location = new System.Drawing.Point(117, 22);
            this.nameText.Name = "nameText";
            this.nameText.Size = new System.Drawing.Size(179, 20);
            this.nameText.TabIndex = 11;
            // 
            // overviewText
            // 
            this.overviewText.Location = new System.Drawing.Point(117, 60);
            this.overviewText.Multiline = true;
            this.overviewText.Name = "overviewText";
            this.overviewText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.overviewText.Size = new System.Drawing.Size(179, 65);
            this.overviewText.TabIndex = 12;
            // 
            // needStatementText
            // 
            this.needStatementText.Location = new System.Drawing.Point(117, 147);
            this.needStatementText.Multiline = true;
            this.needStatementText.Name = "needStatementText";
            this.needStatementText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.needStatementText.Size = new System.Drawing.Size(179, 65);
            this.needStatementText.TabIndex = 13;
            // 
            // objectivesText
            // 
            this.objectivesText.Location = new System.Drawing.Point(117, 228);
            this.objectivesText.Multiline = true;
            this.objectivesText.Name = "objectivesText";
            this.objectivesText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.objectivesText.Size = new System.Drawing.Size(179, 65);
            this.objectivesText.TabIndex = 14;
            // 
            // evalPlanText
            // 
            this.evalPlanText.Location = new System.Drawing.Point(117, 309);
            this.evalPlanText.Multiline = true;
            this.evalPlanText.Name = "evalPlanText";
            this.evalPlanText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.evalPlanText.Size = new System.Drawing.Size(179, 65);
            this.evalPlanText.TabIndex = 15;
            // 
            // budgetText
            // 
            this.budgetText.Location = new System.Drawing.Point(427, 25);
            this.budgetText.Multiline = true;
            this.budgetText.Name = "budgetText";
            this.budgetText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.budgetText.Size = new System.Drawing.Size(179, 65);
            this.budgetText.TabIndex = 16;
            // 
            // timelineText
            // 
            this.timelineText.Location = new System.Drawing.Point(427, 111);
            this.timelineText.Multiline = true;
            this.timelineText.Name = "timelineText";
            this.timelineText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.timelineText.Size = new System.Drawing.Size(179, 65);
            this.timelineText.TabIndex = 17;
            // 
            // notesText
            // 
            this.notesText.Location = new System.Drawing.Point(427, 282);
            this.notesText.Multiline = true;
            this.notesText.Name = "notesText";
            this.notesText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.notesText.Size = new System.Drawing.Size(179, 40);
            this.notesText.TabIndex = 19;
            // 
            // lettersText
            // 
            this.lettersText.Location = new System.Drawing.Point(427, 198);
            this.lettersText.Multiline = true;
            this.lettersText.Name = "lettersText";
            this.lettersText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lettersText.Size = new System.Drawing.Size(179, 65);
            this.lettersText.TabIndex = 18;
            // 
            // projectLabel
            // 
            this.projectLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projectLabel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.projectLabel.Location = new System.Drawing.Point(330, 25);
            this.projectLabel.Multiline = true;
            this.projectLabel.Name = "projectLabel";
            this.projectLabel.ReadOnly = true;
            this.projectLabel.Size = new System.Drawing.Size(91, 51);
            this.projectLabel.TabIndex = 33;
            this.projectLabel.Text = "Organizational Budget:";
            // 
            // AddProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 396);
            this.Controls.Add(this.projectLabel);
            this.Controls.Add(this.lettersText);
            this.Controls.Add(this.notesText);
            this.Controls.Add(this.timelineText);
            this.Controls.Add(this.budgetText);
            this.Controls.Add(this.evalPlanText);
            this.Controls.Add(this.objectivesText);
            this.Controls.Add(this.needStatementText);
            this.Controls.Add(this.overviewText);
            this.Controls.Add(this.nameText);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddProject";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Project Editor";
            this.Load += new System.EventHandler(this.AddProject_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TextBox nameText;
        private System.Windows.Forms.TextBox overviewText;
        private System.Windows.Forms.TextBox needStatementText;
        private System.Windows.Forms.TextBox objectivesText;
        private System.Windows.Forms.TextBox evalPlanText;
        private System.Windows.Forms.TextBox budgetText;
        private System.Windows.Forms.TextBox timelineText;
        private System.Windows.Forms.TextBox notesText;
        private System.Windows.Forms.TextBox lettersText;
        private System.Windows.Forms.TextBox projectLabel;
    }
}