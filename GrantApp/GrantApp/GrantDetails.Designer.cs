namespace GrantApp
{
    partial class GrantDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.nameLabel = new System.Windows.Forms.Label();
			this.backButton = new System.Windows.Forms.Button();
			this.descLabel = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.grantorLabel = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.submitLabel = new System.Windows.Forms.Label();
			this.startLabel = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.paymentLabel = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.turnaroundLabel = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.duedateLabel = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.actualLabel = new System.Windows.Forms.Label();
			this.requestedLabel = new System.Windows.Forms.Label();
			this.statusLabel = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.passwordLabel = new System.Windows.Forms.Label();
			this.usernameLabel = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.writerLabel = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.programLabel = new System.Windows.Forms.TextBox();
			this.projectLabel = new System.Windows.Forms.TextBox();
			this.documentationLabel = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.emphasisLabel = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.notesLabel = new System.Windows.Forms.TextBox();
			this.websiteLabel = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// nameLabel
			// 
			this.nameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nameLabel.Location = new System.Drawing.Point(0, 0);
			this.nameLabel.Name = "nameLabel";
			this.nameLabel.Padding = new System.Windows.Forms.Padding(0, 20, 0, 0);
			this.nameLabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.nameLabel.Size = new System.Drawing.Size(549, 562);
			this.nameLabel.TabIndex = 2;
			this.nameLabel.Text = "Grant Name";
			this.nameLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// backButton
			// 
			this.backButton.Location = new System.Drawing.Point(12, 12);
			this.backButton.Name = "backButton";
			this.backButton.Size = new System.Drawing.Size(75, 23);
			this.backButton.TabIndex = 3;
			this.backButton.Text = "Back";
			this.backButton.UseVisualStyleBackColor = true;
			// 
			// descLabel
			// 
			this.descLabel.AutoSize = true;
			this.descLabel.Location = new System.Drawing.Point(119, 94);
			this.descLabel.Name = "descLabel";
			this.descLabel.Size = new System.Drawing.Size(89, 13);
			this.descLabel.TabIndex = 4;
			this.descLabel.Text = "Grant Description";
			this.descLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 123);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(48, 13);
			this.label1.TabIndex = 5;
			this.label1.Text = "Grantor: ";
			// 
			// grantorLabel
			// 
			this.grantorLabel.AutoSize = true;
			this.grantorLabel.Location = new System.Drawing.Point(119, 123);
			this.grantorLabel.Name = "grantorLabel";
			this.grantorLabel.Size = new System.Drawing.Size(71, 13);
			this.grantorLabel.TabIndex = 6;
			this.grantorLabel.Text = "Grantor Label";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 94);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(63, 13);
			this.label2.TabIndex = 7;
			this.label2.Text = "Description:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 155);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(68, 13);
			this.label3.TabIndex = 8;
			this.label3.Text = "Submit Date:";
			// 
			// submitLabel
			// 
			this.submitLabel.AutoSize = true;
			this.submitLabel.Location = new System.Drawing.Point(119, 155);
			this.submitLabel.Name = "submitLabel";
			this.submitLabel.Size = new System.Drawing.Size(33, 13);
			this.submitLabel.TabIndex = 9;
			this.submitLabel.Text = "None";
			// 
			// startLabel
			// 
			this.startLabel.AutoSize = true;
			this.startLabel.Location = new System.Drawing.Point(119, 259);
			this.startLabel.Name = "startLabel";
			this.startLabel.Size = new System.Drawing.Size(33, 13);
			this.startLabel.TabIndex = 11;
			this.startLabel.Text = "None";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 259);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(58, 13);
			this.label5.TabIndex = 10;
			this.label5.Text = "Start Date:";
			// 
			// paymentLabel
			// 
			this.paymentLabel.AutoSize = true;
			this.paymentLabel.Location = new System.Drawing.Point(119, 296);
			this.paymentLabel.Name = "paymentLabel";
			this.paymentLabel.Size = new System.Drawing.Size(33, 13);
			this.paymentLabel.TabIndex = 13;
			this.paymentLabel.Text = "None";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(12, 296);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(77, 13);
			this.label7.TabIndex = 12;
			this.label7.Text = "Payment Date:";
			// 
			// turnaroundLabel
			// 
			this.turnaroundLabel.AutoSize = true;
			this.turnaroundLabel.Location = new System.Drawing.Point(119, 223);
			this.turnaroundLabel.Name = "turnaroundLabel";
			this.turnaroundLabel.Size = new System.Drawing.Size(33, 13);
			this.turnaroundLabel.TabIndex = 15;
			this.turnaroundLabel.Text = "None";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(12, 223);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(91, 13);
			this.label9.TabIndex = 14;
			this.label9.Text = "Turnaround Date:";
			// 
			// duedateLabel
			// 
			this.duedateLabel.AutoSize = true;
			this.duedateLabel.Location = new System.Drawing.Point(119, 188);
			this.duedateLabel.Name = "duedateLabel";
			this.duedateLabel.Size = new System.Drawing.Size(33, 13);
			this.duedateLabel.TabIndex = 17;
			this.duedateLabel.Text = "None";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(12, 188);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(56, 13);
			this.label11.TabIndex = 16;
			this.label11.Text = "Due Date:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 372);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(79, 13);
			this.label4.TabIndex = 19;
			this.label4.Text = "Actual Amount:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(12, 335);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(101, 13);
			this.label6.TabIndex = 18;
			this.label6.Text = "Requested Amount:";
			// 
			// actualLabel
			// 
			this.actualLabel.AutoSize = true;
			this.actualLabel.Location = new System.Drawing.Point(119, 372);
			this.actualLabel.Name = "actualLabel";
			this.actualLabel.Size = new System.Drawing.Size(66, 13);
			this.actualLabel.TabIndex = 21;
			this.actualLabel.Text = "Actual Label";
			// 
			// requestedLabel
			// 
			this.requestedLabel.AutoSize = true;
			this.requestedLabel.Location = new System.Drawing.Point(119, 335);
			this.requestedLabel.Name = "requestedLabel";
			this.requestedLabel.Size = new System.Drawing.Size(88, 13);
			this.requestedLabel.TabIndex = 20;
			this.requestedLabel.Text = "Requested Label";
			// 
			// statusLabel
			// 
			this.statusLabel.AutoSize = true;
			this.statusLabel.Location = new System.Drawing.Point(119, 409);
			this.statusLabel.Name = "statusLabel";
			this.statusLabel.Size = new System.Drawing.Size(37, 13);
			this.statusLabel.TabIndex = 23;
			this.statusLabel.Text = "Status";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(12, 409);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(40, 13);
			this.label10.TabIndex = 22;
			this.label10.Text = "Status:";
			// 
			// passwordLabel
			// 
			this.passwordLabel.AutoSize = true;
			this.passwordLabel.Location = new System.Drawing.Point(365, 155);
			this.passwordLabel.Name = "passwordLabel";
			this.passwordLabel.Size = new System.Drawing.Size(82, 13);
			this.passwordLabel.TabIndex = 27;
			this.passwordLabel.Text = "Password Label";
			// 
			// usernameLabel
			// 
			this.usernameLabel.AutoSize = true;
			this.usernameLabel.Location = new System.Drawing.Point(365, 123);
			this.usernameLabel.Name = "usernameLabel";
			this.usernameLabel.Size = new System.Drawing.Size(84, 13);
			this.usernameLabel.TabIndex = 26;
			this.usernameLabel.Text = "Username Label";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(258, 155);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(98, 13);
			this.label13.TabIndex = 25;
			this.label13.Text = "Website Password:";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(258, 123);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(100, 13);
			this.label14.TabIndex = 24;
			this.label14.Text = "Website Username:";
			// 
			// writerLabel
			// 
			this.writerLabel.AutoSize = true;
			this.writerLabel.Location = new System.Drawing.Point(365, 188);
			this.writerLabel.Name = "writerLabel";
			this.writerLabel.Size = new System.Drawing.Size(64, 13);
			this.writerLabel.TabIndex = 29;
			this.writerLabel.Text = "Writer Label";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(258, 188);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(67, 13);
			this.label12.TabIndex = 28;
			this.label12.Text = "Grant Writer:";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(258, 223);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(54, 13);
			this.label15.TabIndex = 30;
			this.label15.Text = "Programs:";
			// 
			// programLabel
			// 
			this.programLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.programLabel.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.programLabel.Location = new System.Drawing.Point(368, 223);
			this.programLabel.Multiline = true;
			this.programLabel.Name = "programLabel";
			this.programLabel.ReadOnly = true;
			this.programLabel.Size = new System.Drawing.Size(169, 51);
			this.programLabel.TabIndex = 31;
			this.programLabel.Text = "Programs Label";
			// 
			// projectLabel
			// 
			this.projectLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.projectLabel.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.projectLabel.Location = new System.Drawing.Point(368, 335);
			this.projectLabel.Multiline = true;
			this.projectLabel.Name = "projectLabel";
			this.projectLabel.ReadOnly = true;
			this.projectLabel.Size = new System.Drawing.Size(169, 51);
			this.projectLabel.TabIndex = 32;
			this.projectLabel.Text = "Projects Label";
			// 
			// documentationLabel
			// 
			this.documentationLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.documentationLabel.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.documentationLabel.Location = new System.Drawing.Point(368, 409);
			this.documentationLabel.Multiline = true;
			this.documentationLabel.Name = "documentationLabel";
			this.documentationLabel.ReadOnly = true;
			this.documentationLabel.Size = new System.Drawing.Size(169, 77);
			this.documentationLabel.TabIndex = 33;
			this.documentationLabel.Text = "Documentation Label";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(258, 297);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(97, 13);
			this.label8.TabIndex = 34;
			this.label8.Text = "Program Emphasis:";
			// 
			// emphasisLabel
			// 
			this.emphasisLabel.AutoSize = true;
			this.emphasisLabel.Location = new System.Drawing.Point(365, 297);
			this.emphasisLabel.Name = "emphasisLabel";
			this.emphasisLabel.Size = new System.Drawing.Size(81, 13);
			this.emphasisLabel.TabIndex = 35;
			this.emphasisLabel.Text = "Emphasis Label";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(258, 334);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(48, 13);
			this.label16.TabIndex = 36;
			this.label16.Text = "Projects:";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(258, 410);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(62, 13);
			this.label17.TabIndex = 37;
			this.label17.Text = "Doc Types:";
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(12, 492);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(38, 13);
			this.label18.TabIndex = 38;
			this.label18.Text = "Notes:";
			// 
			// notesLabel
			// 
			this.notesLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.notesLabel.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.notesLabel.Location = new System.Drawing.Point(122, 492);
			this.notesLabel.Multiline = true;
			this.notesLabel.Name = "notesLabel";
			this.notesLabel.ReadOnly = true;
			this.notesLabel.Size = new System.Drawing.Size(415, 57);
			this.notesLabel.TabIndex = 39;
			this.notesLabel.Text = "Notes Label";
			// 
			// websiteLabel
			// 
			this.websiteLabel.AutoSize = true;
			this.websiteLabel.Location = new System.Drawing.Point(365, 94);
			this.websiteLabel.Name = "websiteLabel";
			this.websiteLabel.Size = new System.Drawing.Size(75, 13);
			this.websiteLabel.TabIndex = 41;
			this.websiteLabel.Text = "Website Label";
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(258, 94);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(49, 13);
			this.label20.TabIndex = 40;
			this.label20.Text = "Website:";
			// 
			// GrantDetails
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(549, 562);
			this.Controls.Add(this.websiteLabel);
			this.Controls.Add(this.label20);
			this.Controls.Add(this.notesLabel);
			this.Controls.Add(this.label18);
			this.Controls.Add(this.label17);
			this.Controls.Add(this.label16);
			this.Controls.Add(this.emphasisLabel);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.documentationLabel);
			this.Controls.Add(this.projectLabel);
			this.Controls.Add(this.programLabel);
			this.Controls.Add(this.label15);
			this.Controls.Add(this.writerLabel);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.passwordLabel);
			this.Controls.Add(this.usernameLabel);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.statusLabel);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.actualLabel);
			this.Controls.Add(this.requestedLabel);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.duedateLabel);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.turnaroundLabel);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.paymentLabel);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.startLabel);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.submitLabel);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.grantorLabel);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.descLabel);
			this.Controls.Add(this.backButton);
			this.Controls.Add(this.nameLabel);
			this.Name = "GrantDetails";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Grant";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Label descLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label grantorLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label submitLabel;
        private System.Windows.Forms.Label startLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label paymentLabel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label turnaroundLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label duedateLabel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label actualLabel;
        private System.Windows.Forms.Label requestedLabel;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label writerLabel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox programLabel;
        private System.Windows.Forms.TextBox projectLabel;
        private System.Windows.Forms.TextBox documentationLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label emphasisLabel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox notesLabel;
        private System.Windows.Forms.Label websiteLabel;
        private System.Windows.Forms.Label label20;
    }
}